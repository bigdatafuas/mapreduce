# Install Hadoop
```
wget https://downloads.apache.org/hadoop/common/hadoop-3.3.1/hadoop-3.3.1.tar.gz
tar xvzf hadoop-3.3.1.tar.gz 
sudo apt install openjdk-11-jdk maven -y

```

# Build and run the project
```
# Generate the input data
echo "Hello Hallo Bye" > input.txt
echo "Hello Hallo Goodbye" > input2.txt
# Build the project
mvn clean install
# Remove the output directory
rm -rf wcout
# Run hadoop
~/hadoop-3.3.1/bin/hadoop jar target/wordcount-1.0-SNAPSHOT.jar com.bigdata.wordcount.WordCount input.txt input2.txt wcout
# Check the output
cat wcout/part-r-00000
```